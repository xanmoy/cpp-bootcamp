#include <iostream>
#include <string>
using namespace std;

class Employee
{
public:
    string name;
    int salary;

    Employee(string n, int s, int sp)
    {
        this->name = n;
        this->salary = s;
        this->secrectPassword = sp;
    }
    void printDetails()
    {
        cout << "The name of our first employee is " << this->name << " and His salaray is " << this->salary << "$ " << endl;
    }
    void getSecrectPassword()
    {
        cout << "The secrect password of employee is " << this->secrectPassword << endl;
    }

private:
    int secrectPassword;
};

class Programmer: public Employee
{
    public:
    int errors;
};

int main()
{
    Employee har("Xanmoy constructor", 344, 323545);
    // har.name = "Xanmoy";
    // har.salary = 100;
    har.printDetails();
    har.getSecrectPassword();
    return 0;
}